Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :user_entities, only: %i[create destroy], param: :access_key
  get ':access_key' => 'user_entities#show'
end
