require 'swagger_helper'

RSpec.describe 'api/user_entities', type: :request do
  path '/user_entities' do
    post 'Creates ' do
      tags 'Create access_key'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user_entity, in: :body, schema: {
        type: :object,
        properties: {
          information: { type: :string },
          available_days: { type: :integer },
          destroy_option: { type: :boolean },
          reviews: { type: :integer }
        },
        required: %w[information destroy_option reviews]
      }

      response '200', 'secret message created' do
        let(:user_entity) {
          { information: 'Find me',
            available_days: 4,
            destroy_option: 'true',
            reviews: 1 }
        }
        run_test!
      end

      response '422', 'invalid info' do
        let(:user_entity) { 
          { information: 'fu',
            available_days: 4,
            destroy_option: 'true',
            reviews: 2 } }
        run_test!
      end
    end
  end

  path '/user_entities/{access_key}' do
    delete 'Destroy saved message' do
      tags 'destroy saved message'
      produces 'application/json', 'application/xml'
      parameter name: :access_key, in: :path, type: :string

      response '200', 'No content' do
        let(:access_key) { create(:user_entity).access_key }
        let(:message) { 'removed' }
        run_test!
      end

      response '404', :not_found do
        let(:access_key) { 'invalid' }
        run_test!
      end
    end
  end
end
