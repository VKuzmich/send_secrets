# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

RSpec.describe CleanDbWorker, type: :worker do

  it 'receives jobs to do' do
    expect {CleanDbWorker.perform_async(1, 2)
    }.to change(CleanDbWorker.jobs, :size).by(1)
  end

  it 'is retryable' do
    expect(CleanDbWorker).to be_retryable true
  end

  it 'is expected to be performed in 1 minute' do
    time = 1.minutes.from_now
    CleanDbWorker.perform_at time, 'CleanDbWorker', true
    expect(CleanDbWorker).to have_enqueued_sidekiq_job('CleanDbWorker', true).at(time)
  end
end
