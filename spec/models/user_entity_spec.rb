# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserEntity, type: :model do
  describe 'validations' do

    # create(UserEntity.)
    it { is_expected.to validate_presence_of(:information) }
    it { is_expected.to validate_length_of(:information).with_message('too short message') }
    it { is_expected.to validate_length_of(:information).with_message('too long message') }


  end

end
