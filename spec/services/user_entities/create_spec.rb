# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserEntities::Create, type: :services do
  let(:valid_params) {
    { information: 'mme',
      destroy_option: 'true',
      reviews: '3',
      available_days: 4 }
  }

  let(:invalid_params) {
    { information: 'me',
      destroy_option: 'true',
      reviews: '3',
      available_days: 4 }
  }

  let(:result) {
    { information: 'mdde',
      access_key: 'kYh',
      termination: '2020-08-08 06:55:57.062170',
      destroy_option: false,
      reviews: 5,
      created_at: '2020-08-04 06:55:57.063206',
      updated_at: '2020-08-04 06:55:57.063206' }
  }

  let(:invalid_result) { false }

  context '.call' do
    it 'valid params creates data' do
      ActionController::Parameters.permit_all_parameters = true
      params = ActionController::Parameters.new(valid_params)
      fake_create = double(:fake_entity)
      allow(UserEntities::Create).to receive(:new).with(params).and_return(fake_create)
      allow(fake_create).to receive(:call).and_return(result)
      new_result = fake_create.call
      expect(new_result).to eq(result)
      expect(new_result[:access_key]).to eq('kYh')
      expect(new_result[:termination]).to eq('2020-08-08 06:55:57.062170')
      expect(new_result[:information]).to eq('mdde')
      expect(new_result[:destroy_option]).to eq(false)
      expect(new_result[:reviews]).to eq(5)
      expect(new_result[:created_at]).to eq('2020-08-04 06:55:57.063206')
      expect(new_result[:updated_at]).to eq('2020-08-04 06:55:57.063206')
    end

    it 'invalid params raises false' do
      ActionController::Parameters.permit_all_parameters = true
      params = ActionController::Parameters.new(invalid_params)
      fake_create = double(:fake_entity)
      allow(UserEntities::Create).to receive(:new).with(params).and_return(fake_create)
      allow(fake_create).to receive(:call).and_return(invalid_result)
      new_result = fake_create.call
      expect(new_result).to eq(false)
    end
  end
end
