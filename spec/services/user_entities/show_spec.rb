# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserEntities::Show, type: :services do
  let(:valid_params) {
    { information: 'mme',
      destroy_option: 'true',
      reviews: '3',
      available_days: 4,
      access_key: 'wer'
    }}

  let(:results) {
      {
        information: 'dme',
        access_key: 'wer',
        termination: '2020-12-06 13:03:19',
        destroy_option: false,
        reviews: 2}
  }


  let(:invalid_params) {
    { information: 'me',
      destroy_option: 'true',
      reviews: '3',
      available_days: 4
    }}
  let(:valid_access_key) { 'wer'}
  let(:invalid_access_key) { 'wr'}
  before do
    encrypter = double(:fake_encrypter)
    allow(Encrypter).to receive(:new).and_return(encrypter)
    allow(encrypter).to receive(:generate_characters).and_return(valid_access_key)
  end

  context '.call' do
    it 'finds record' do
      fake_create = double(:fake_entity)
      allow(UserEntities::Show).to receive(:new).with(:valid_access_key).and_return(fake_create)
      allow(fake_create).to receive(:call).and_return(results)
      new_result = fake_create.call
      expect(new_result[:information]).to eq('dme')
      expect(new_result[:termination]).to eq('2020-12-06 13:03:19')
      expect(new_result[:destroy_option]).to eq(false)
      expect(new_result[:reviews]).to eq(2)
    end

    it 'invalid record raises error' do
      expect_any_instance_of(UserEntities::Show).to receive(:call).and_return(false)
      UserEntities::Show.new(access_key: valid_params[:invalid_access_key]).call
    end
  end
end
