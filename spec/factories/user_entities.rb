require 'date'

FactoryBot.define do
  factory :user_entity do |f|
    f.information { FFaker::Lorem.phrase[3..20] }
    f.destroy_option { [true, false].sample }
    f.reviews { rand(1..10) }
    f.termination { Time.now + 3 }

  end
end
