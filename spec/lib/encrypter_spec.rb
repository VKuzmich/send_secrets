# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Encrypter, type: :lib do
  ALLOWED_LETTERS = %w[a c d e f h j k m n p q r t u v w x y].freeze
  ALLOWED_NUMBERS = %w[2 3 4 5 6 7 8 9].freeze
  ALLOWED_CAPITALS = %w[A C D E F H J K L M N P Q R T U V W X Y].freeze
  ALLOWED_SYMBOLS = %w[].freeze
  DEFAULT_NUMBERS = 8

  describe 'initialization' do
    let(:encrypter) { Encrypter.new }
    let(:users_number_of_symbols) { 5 }
    let(:default_number_of_symbols) { 8 }
    let(:zero_number_of_symbols) { 0 }
    let(:negative_number_of_symbols) { -5 }

    context 'initializer' do
      it 'generate_characters gets empty array by default' do
        expect(encrypter.characters).to eq([])
      end
    end

    context 'checking .generate_characters with the value of numbers of symbols' do
      it 'receives default numbers of symbols' do
        d = encrypter.generate_characters(default_number_of_symbols)
        expect(d.length).to eq DEFAULT_NUMBERS
      end

      it ' receives users numbers of symbols' do
        d = encrypter.generate_characters(users_number_of_symbols)
        expect(d.length).to eq 5
      end

      it 'receives zero numbers of symbols and raise ArgumentError' do
        expect do
          encrypter.generate_characters(zero_number_of_symbols)
        end.to raise_error(ArgumentError)
      end

      it 'receives negative symbols numbers and raise ArgumentError' do
        expect do
          encrypter.generate_characters(negative_number_of_symbols)
        end.to raise_error(ArgumentError)
      end
    end
  end
end
