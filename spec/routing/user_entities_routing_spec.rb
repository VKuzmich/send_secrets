# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserEntitiesController, type: :routing do
  it do
    expect(get: '/access_key')
        .to route_to('user_entities#show', access_key: 'access_key')
  end
  it do
    expect(post: '/user_entities')
        .to route_to('user_entities#create')
  end
  it do
    expect(delete: '/user_entities/access_key')
        .to route_to('user_entities#destroy', access_key: 'access_key')
  end
end
