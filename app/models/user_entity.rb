# frozen_string_literal: true

require 'date'

class UserEntity < ApplicationRecord
  validates :information,
            presence: true,
            length: { within: 3..30,
                      too_short: 'too short message',
                      too_long: 'too long message' }

  validates_datetime :termination, after: -> { Time.now }

  validates :destroy_option, inclusion: { in: [true, false] }

  validates :reviews,
            numericality: { only_integer: true,
                            greater_than_or_equal_to: 1,
                            less_than_or_equal_to: 10,
                            message: 'Valid number required from 1 to 10' }

  before_create :set_access_key, :reviews

  def set_access_key
    self.access_key = generate_access_key
  end

  def generate_access_key
    counter = total_count = 0
    number_of_chars = ENV.fetch('NUMBER_OF_CHARS', 3).to_i
    encrypter = Encrypter.new

    begin
      counter += 1
      total_count += 1
      access_key = encrypter.generate_characters(number_of_chars)
      if counter >= 25
        number_of_symbols += 1
        counter = 0
      end
    end while UserEntity.where(access_key: access_key).exists?
    access_key
  end
end
