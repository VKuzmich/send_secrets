# frozen_string_literal: true

class UserEntitiesController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def create
    @result = UserEntities::Create.new(params).call
    return render '/422.json.jbuilder', status: :unprocessable_entity unless @result
  end

  def show
    @result = UserEntities::Show.new(params).call
    return render '/404.json.jbuilder', status: :not_found unless @result
  end

  def destroy
    @result = UserEntities::Destroy.new(params).call
    return render '/404.json.jbuilder', status: :not_found unless @result
  end

  private

  def record_not_found
    render json: { error: '404 Not Found' }, status: :not_found
  end
end
