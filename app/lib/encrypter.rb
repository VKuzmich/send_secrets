# frozen_string_literal: true

class Encrypter
  ALLOWED_LETTERS = %w[a c d e f h j k m n p q r t u v w x y].freeze
  ALLOWED_NUMBERS = %w[2 3 4 6 7 8 9].freeze
  ALLOWED_CAPITALS = %w[A C D E F H J K L M N P Q R T U V W X Y].freeze
  # ALLOWED_SYMBOLS = %w[^ * < >].freeze
  DEFAULT_NUMBERS = 16

  attr_accessor :characters

  def initialize
    super
    @characters = []
  end

  def generate_characters(number_of_characters = DEFAULT_NUMBERS)
    raise(ArgumentError, 'negative array size') if number_of_characters <= 0

    @characters = [*ALLOWED_LETTERS,
                   *ALLOWED_NUMBERS,
                   *ALLOWED_CAPITALS,
                   # *ALLOWED_SYMBOLS
    ]
                  .sample(number_of_characters)
    @characters.join
  end
end
