module UserEntities
  class Destroy
    def initialize(params)
      @params = params
    end

    def call
      fetch_entity
      return not_allowed_result if @user_entity.destroy_option == false

      @user_entity.destroy
      successfull_result
    end

    private
    def successfull_result
      {
        response: {message: 'removed'},
        status: 200
      }
    end

    def not_allowed_result
      {
        response: { message: 'Not allowed' },
        status: :method_not_allowed
      }
    end

    def fetch_entity
      @user_entity = UserEntity.find_by(access_key: @params[:access_key])
      raise ActiveRecord::RecordNotFound unless @user_entity
    end
  end
end



