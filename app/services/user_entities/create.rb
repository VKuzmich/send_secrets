module UserEntities
  class Create
    attr_accessor :params
    def initialize(params)
      @params = params
    end

    def call
      @user_entity = UserEntity.new(user_entity_params)
      set_time

      if @user_entity.save
        @user_entity
      else
        error_result
      end
    end

    private

    def error_result
      false
    end

    def user_entity_params
      @params.require(:user_entity).permit(:information,
                                           :destroy_option,
                                           :reviews)
    end

    def set_time
      if @params[:available_days].present?
        @user_entity.termination = Time.now + @params[:available_days].days
      end
    end
  end
end

