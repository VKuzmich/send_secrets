module UserEntities
  class Show
    def initialize(params)
      @params = params
    end

    def call
      fetch_entity
      return not_found_result if @user_entity.reviews <= 0

      decrement_reviews
      @user_entity
    end

    private

    def decrement_reviews
      @user_entity.decrement!(:reviews)
    end

    def not_found_result
      false
    end

    def fetch_entity
      @user_entity = UserEntity.find_by(access_key: @params[:access_key])
      raise ActiveRecord::RecordNotFound unless @user_entity
    end
  end
end