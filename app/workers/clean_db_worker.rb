class CleanDbWorker
  include Sidekiq::Worker

  def perform(*args)
    delete_expired_messages
    p 'successfully deleted!'
  end

  def delete_expired_messages
    today = (20.days.ago).to_s
    expired_entities = UserEntity.where('termination < ?', today)
    expired_entities.destroy_all if expired_entities.exists?
  end
end
