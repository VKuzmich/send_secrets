class CreateUserEntities < ActiveRecord::Migration[6.0]
  def change
    create_table :user_entities do |t|
      t.string    :information
      t.string    :access_key
      t.timestamp :termination
      t.boolean   :destroy_option
      t.integer   :reviews

      t.timestamps
    end
  end
end
